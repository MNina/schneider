<!DOCTYPE html>
<html lang="rs">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Schneider auto centar | Beograd | Registracija automobila | Pranje automobila | Kafic | Tehnicki pregled</title>
        <link href="main.css" rel="stylesheet" type="text/css">
        <link href="includes/kafe.css" rel="stylesheet" type="text/css">

        <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
        
       <meta name="keywords" content="schneider, auto, centar, pranje, automobila, registracija, tehnicki, pregled, auto ,rakovica, beograd">
       <meta name="description" content="Schneider auto centar d.o.o nalazi se u ulici Patrijarha Dimitrija 1a, 11090, Beograd. Nudimo usluge registracije vozila, osiguranje, tehnički pregled, pranje automobila. Dok se naš stručni tim bavi Vašim vozilom možete uživati u prijatnom ambijentu našeg kafića.">
       <meta name="author" content="MDev">
        
    </head>

    <main>
         <!--HEADER-->
         <?php 
                include('includes/inc-header.php');
         ?>
          <!--HEADER End-->


         <!--Section A kafe-->
         <div class="kafe-main grid">
            
            <!-- <h1>Pranje vozila</h1> -->
            <picture>
                    <source media="(min-width: 768px)"srcset="images/kafe/schneider-tehnicki-pregled-kafe.jpg">
                   
                    <img src="images/kafe/schneider-tehnicki-pregled-kafe-mobil.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
            </picture>
         </div>       
           <!--section A kafe End-->
          
           <!--section B kafe End-->
             <div class="section-b-kafe">
              <div class="container">
                     <div class="b-main-kafe grid">
                         <div class="b-box-kafe-1">
                            <img src="images/kafe/schneider-tehnicki-pregled-kafe-texnicki pregled.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                         </div>
                         <div class="b-box-kafe-2">
                                <h2>Dobro došli u Schneider Intermezzo kafe</h2>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Autem dolores nostrum, 
                                       animi modi quod possimus necessitatibus quaerat sequi nihil repudiandae, soluta 
                                       placeat eum nulla iste perferendis sint temporibus. Explicabo, ex?
                                </p>
                                
                         </div>
                     </div>
              </div>

             </div>

           <!--section B kafe End-->

           <!--section C kafe -->

              <div class="section-c-kafe grid">
                     <div class="c-box-kafe">
                        <img src="images/kafe/schneider-tehnicki-pregled-kafe-galerija-1.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                     </div>
                     <div class="c-box-kafe">
                        <img src="images/kafe/schneider-tehnicki-pregled-kafe-galerija-2.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                     </div>
                     <div class="c-box-kafe">
                        <img src="images/kafe/schneider-tehnicki-pregled-kafe-galerija-3.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                     </div>
                     <div class="c-box-kafe">
                        <img src="images/kafe/schneider-tehnicki-pregled-kafe-galerija-4.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                     </div>
                     <div class="c-box-kafe">
                        <img src="images/kafe/schneider-tehnicki-pregled-kafe-galerija-5.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                     </div>
                     <div class="c-box-kafe">
                        <img src="images/kafe/schneider-tehnicki-pregled-kafe-galerija-6.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                     </div>
              </div>

           <!--section C kafe End-->

      

      <!--FOOTER-->
         <?php 
                include('includes/inc-footer.php');
            ?>
      <!--FOOTER End-->

      

    </main>


    <script src="main.js"></script>
</html>

