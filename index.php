<!DOCTYPE html>
<html lang="rs">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Schneider auto centar | Beograd | Registracija automobila | Pranje automobila | Kafic | Tehnicki pregled</title>
        <link href="main.css" rel="stylesheet" type="text/css">
        <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
        
       <meta name="keywords" content="schneider, auto, centar, pranje, automobila, registracija, tehnicki, pregled, auto ,rakovica, beograd">
       <meta name="description" content="Schneider auto centar d.o.o nalazi se u ulici Patrijarha Dimitrija 1a, 11090, Beograd. Nudimo usluge registracije vozila, osiguranje, tehnički pregled, pranje automobila. Dok se naš stručni tim bavi Vašim vozilom možete uživati u prijatnom ambijentu našeg kafića.">
       <meta name="author" content="MDev">
        
    </head>

    <main>
         <!--HEADER-->
         <?php 
                include('includes/inc-header.php');
         ?>
          <!--HEADER End-->


         <!--Section A-->
         <?php 
                include('includes/inc-section-a.php');
         ?>
         <!--section A End-->


        <!--Section B-->
         <?php 
                include('includes/inc-section-b.php');
         ?>
        
        <!--section B End-->


        <!--Section C-->
         <?php 
                include('includes/inc-section-c.php');
         ?>
        <!--section C End-->


        <!--Section D-->
            <?php 
                include('includes/inc-section-d.php');
            ?>
        <!--section D End-->

      <!--FOOTER-->
         <?php 
                include('includes/inc-footer.php');
            ?>
      <!--FOOTER End-->

      

    </main>


    <script src="main.js"></script>
</html>

