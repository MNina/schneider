     <a href="tel:+381123456" class="mobile-phone-btn"><i class="fas fa-phone"></i></a>
     
     <header class="header-container" id="deloviStickyNav">
        <div class="container">
            <div class="main-header grid">
                    <div class="header-logo">
                        <a href="#"><img class="header-logo-img" src="images/schneider-auto-centar-doo.png"></a>
                    </div>
                    <div class="hamburger">
                      <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                          <i class="fa fa-bars manu-hamburger"></i>
                      </a>
                    </div>
                    
                    <div id="main-nav" class="main-nav">
                        <ul class="grid">
                            <li><a  class="nav-link nav-link-grow-up" href="index.php">Početna</a></li>
                            <li><a class="nav-link nav-link-grow-up" href="tehnicki.php">Tehnički pregled</a></li>
                            <li><a class="nav-link-grow-up" href="pranje.php">Pranje vozila</a></li>
                            <li><a class="nav-link-grow-up" href="kafic.php">Kafić</a></li>
                            <li><a class="nav-link-grow-up" href="kontakt.php">Kontakt</a></li>
                        </ul>
                    </div>
             </div>
        </div> <!-- .main-header -->   
     </header>