  <!--Section B-->
  <div class="section-b">
           <!-- <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 319">
             <path fill="#fff" fill-opacity="1" d="M0,128L48,149.3C96,171,192,213,288,218.7C384,224,480,192,576,154.7C672,117,768,75,864,69.3C960,64,1056,96,1152,101.3C1248,107,1344,85,1392,74.7L1440,64L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"></path> 
            </svg> -->
                <div class="container">
                    <div class="main-b grid">
                      <h2 class="text-center">Schneider auto centar pruža usluge</h2>
                       <ul>
                          <li>Registracije svih vrsta motornih vozila, automobila, kamiona ili motora</li>
                          <li>Tehnicki pregled vozila</li>
                          <li>Uslužno pranje </li>
                          <li>Izradu osiguranja</li>
                          <li>Izdavanje zelenog kartona</li>
                        </ul>
                      <p>Kod nas ste u mogućnosti da kvalitetno i na jednom mestu završite kompletnu registraciju vašeg vozila u najkraćem roku.
                         U našoj poslovnici moguće je obaviti i uslužno pranje vozila.
                         U cilju naše efikasnosti i poštovanja vremena svih naših klijenata, potrebno je zakazati termin i ispoštovati isti.
                       
                        </p>
                      
                    </div>

                </div>
                 <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 319"><path fill="#fff" fill-opacity="1" d="M0,128L48,149.3C96,171,192,213,288,218.7C384,224,480,192,576,154.7C672,117,768,75,864,69.3C960,64,1056,96,1152,101.3C1248,107,1344,85,1392,74.7L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></path></svg>
               
           </div>
        <!--section B End-->