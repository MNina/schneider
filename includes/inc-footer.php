<footer class="footer">
           <div class="main-footer container grid">
              <div class="logo-fu">
                    <a href="#"><img src="images/schneider-auto-centar-doo.png" class="header-logo-img"></a>
              </div>
              <div class="contact-fu">
                <div class="tel-fu">
                    <i class="fas fa-phone fu-phone"></i>
                    <p><a href="tel:+3810123456789">012 345 67 89</a></p>
                </div>
                <div class="adres-fu">
                    <i class="fas fa-map-marker-alt fu-map"></i>
                    <p>Patrijarha Dimitrija 1/A</p>
                    <p>11090 Beograd (Rakovica)</p> 
                </div>
              </div>
            
              <!-- <div class="box-fu grid">
                  <ul>
                      <li><a href="#">Početna</a></li>
                      <li><a href="#">Tehnički pregled</a></li>
                      <li><a href="#">Pranje vozila</a></li>
                      <li><a href="#">Kafić</a></li>
                      <li><a href="#">Kontakt</a></li>
                   </ul>
               </div>  -->
            </div>

        </footer>