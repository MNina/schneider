     <!-- Section A-->
     <div class="slide-container">
              <div class="slideshow grid">
                  <div class="slide fade">
                  <picture>
                       <source media="(max-width: 500px)" srcset="images/schneider-auto-registracija-vozila11.jpg">
                       <source media="(max-width: 501px)" srcset="images/schneider-auto-registracija-vozila1.jpg">
                       <img src="images/schneider-auto-registracija-vozila1.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                    </picture>
                    <div class="slide-text"><h1>REGISTRACIJA VOZILA</h1></div>
                  </div>
                  <div class="slide fade">
                    <picture>
                       <source media="(max-width: 500px)" srcset="images/schneider-auto-registracija-vozila22.jpeg">
                       <source media="(max-width: 501px)" srcset="images/schneider-auto-registracija-vozila2.jpg">
                       <img src="images/schneider-auto-registracija-vozila2.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                    </picture>
                    <div class="slide-text"><h1>TEHNIČKI PREGLED</h1></div>
                  </div>
                  <div class="slide fade">
                    <picture>
                        <source media="(max-width: 500px)" srcset="images/schneider-auto-registracija-vozila33.jpg">
                        <source media="(max-width: 501px)" srcset="images/schneider-auto-registracija-vozila3.jpg">
                        <img src="images/schneider-auto-registracija-vozila3.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                    </picture>
                    
                    <div class="slide-text"><h1>USLUŽNO PRANJE</h1></div>
                  </div>
                  <div class="slide fade">
                      <picture>
                          <source media="(max-width: 500px)" srcset="images/schneider-auto-registracija-vozila44.jpg">
                          <source media="(max-width: 501px)" srcset="images/schneider-auto-registracija-vozila4.jpg">
                          <img src="images/schneider-auto-registracija-vozila4.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                      </picture>
                      <div class="slide-text"><h1>PRIJATAN AMBIJENT KAFIĆA</h1></div>
                  </div>

                   <!-- Next and previous buttons -->
                   <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                    <a class="next" onclick="plusSlides(1)">&#10095;</a>

                </div>
            
        
           </div>
        <!-- Section A END-->