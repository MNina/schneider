<!DOCTYPE html>
<html lang="rs">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Schneider auto centar | Beograd | Registracija automobila | Pranje automobila | Kafic | Tehnicki pregled</title>
        <link href="main.css" rel="stylesheet" type="text/css">
        <link href="includes/tehnicki.css" rel="stylesheet" type="text/css">

        <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
        
       <meta name="keywords" content="schneider, auto, centar, pranje, automobila, registracija, tehnicki, pregled, auto ,rakovica, beograd">
       <meta name="description" content="Schneider auto centar d.o.o nalazi se u ulici Patrijarha Dimitrija 1a, 11090, Beograd. Nudimo usluge registracije vozila, osiguranje, tehnički pregled, pranje automobila. Dok se naš stručni tim bavi Vašim vozilom možete uživati u prijatnom ambijentu našeg kafića.">
       <meta name="author" content="MDev">
        
    </head>

    <main>
         <!--HEADER-->
         <?php 
                include('includes/inc-header.php');
         ?>
          <!--HEADER End-->


           <!--Section A-->
           <?php 
                include('includes/inc-section-a.php');
         ?>
         <!--section A End-->
          
           <!--section B tehnicki End-->
             <div class="section-b-tehnicki">
              <div class="container">
                     <div class="b-main-tehnicki grid">
                         <div class="b-box-tehnicki-1">
                            <img src="images/tehnicki/schneider-auto-centar-doo-registracija1.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                         </div>
                         <div class="b-box-tehnicki-2">
                                <h2>Tehnički pregledi i registracija vozila</h2>
                                <h3>Lorem ipsum dolor sit amet consectetur adipisicing.</h3>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Autem dolores nostrum, 
                                       animi modi quod possimus necessitatibus quaerat sequi nihil repudiandae, soluta 
                                       placeat eum nulla iste perferendis sint temporibus. Explicabo, ex?
                                </p>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minus, adipisci qui reprehenderit deleniti explicabo laboriosam fuga ipsa commodi 
                                    vero sapiente excepturi ipsum dignissimos id delectus. Vitae laudantium voluptas cupiditate dicta consequuntur, eius voluptatum, hic commodi 
                                    alias distinctio molestias maxime qui?
                                </p>
                                <p>Lorem ipsum dolor sit amet.</p>
                                <p>Lorem ipsum dolor sit amet.</p>
                                <p>Lorem ipsum dolor sit amet.</p>
                                <p>Lorem ipsum dolor sit amet.</p>
                                <p>Lorem ipsum dolor sit amet.</p>
                         </div>
                     </div>
              </div>

             </div>

           <!--section B tehnicki End-->

           <!--section C tehnicki -->

              <div class="section-c-tehnicki">
                  <div class="container">
                      <div class="c-main-tehnicki grid">
                            <div class="c-box-tehnicki">
                                <img src="images/tehnicki/schneider-auto-centar-doo-registracija-tehnicki-galerija-1.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                            </div>
                            <div class="c-box-tehnicki">
                                <img src="images/tehnicki/schneider-auto-centar-doo-registracija-tehnicki-galerija-2.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                            </div>
                            <div class="c-box-tehnicki">
                                <img src="images/tehnicki/schneider-auto-centar-doo-registracija-tehnicki-galerija-3.jpeg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                            </div>
                            <div class="c-box-tehnicki">
                                <img src="images/tehnicki/schneider-auto-centar-doo-registracija-tehnicki-galerija-4.jpeg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                            </div>
                            <!-- <div class="c-box-tehnicki">
                                <img src="images/tehnicki/schneider-auto-centar-doo-registracija-tehnicki-galerija-5.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                            </div>
                            <div class="c-box-tehnicki">
                                <img src="images/tehnicki/schneider-auto-centar-doo-registracija-tehnicki-galerija-6.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                            </div> -->
                      </div>
                  </div>
              </div>

           <!--section C tehnicki End-->

      

      <!--FOOTER-->
         <?php 
                include('includes/inc-footer.php');
            ?>
      <!--FOOTER End-->

      

    </main>


    <script src="main.js"></script>
</html>

