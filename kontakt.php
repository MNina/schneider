<!DOCTYPE html>
<html lang="rs">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Schneider auto centar | Beograd | Registracija automobila | Pranje automobila | Kafic | Tehnicki pregled</title>
        <link href="main.css" rel="stylesheet" type="text/css">
        <link href="includes/kontakt.css" rel="stylesheet" type="text/css">

        <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
        
       <meta name="keywords" content="schneider, auto, centar, pranje, automobila, registracija, tehnicki, pregled, auto ,rakovica, beograd">
       <meta name="description" content="Schneider auto centar d.o.o nalazi se u ulici Patrijarha Dimitrija 1a, 11090, Beograd. Nudimo usluge registracije vozila, osiguranje, tehnički pregled, pranje automobila. Dok se naš stručni tim bavi Vašim vozilom možete uživati u prijatnom ambijentu našeg kafića.">
       <meta name="author" content="MDev">
        
    </head>

    <main>
         <!--HEADER-->
         <?php 
                include('includes/inc-header.php');
         ?>
          <!--HEADER End-->


         <!--Section A-->
            
            <div class="video-kontakt">
            
             <video src="images/Car - 11490 (1).mp4" muted loop autoplay></video>
             <div class="overlay"></div>
                <div class="video-text">
                        <h2>Zakažite tehnicki pregled i obavite registraciju brzo i lako</h2>
                        <div class="video-tel">
                            <i class="fas fa-phone"></i>
                            <p><a href="tel:+3810123456789">012 345 67 89</a></p>
                        </div>
                </div>
            </div>
         <!--section A End-->

         <!--Section B- KONTAKT-->
            <div class="container kontakt-b">
             
               <div class="grid kontakt-info">
                    <div class="podaci-kontakt">
                        <h1>Zakažite tehnički pregled</h1>
                    <div class="contact-k">
                            <div class="tel-k">
                                <i class="fas fa-phone"></i>
                                <p><a href="tel:+3810123456789">012 345 67 89</a></p>
                            </div>
                            <div class="adres-k">
                                <i class="fas fa-map-marker-alt"></i>
                                <p>Patrijarha Dimitrija 1/A</p>
                                <p>11090 Beograd (Rakovica)</p> 
                            </div>
                        </div> 
                    </div>
                    <div class="radno-vreme">
                        <h3>Radno vreme</h3>
                        <p>Ponedeljak : 9-17h</p>
                        <p>Utorak: 9h-17h</p>
                        <p>Sreda: 9h-17h</p>
                        <p>Četvrtak: 9h-17h</p>
                        <p>Petak: 9h-17h</p>
                        <p>Subota: 9h-14h</p>
                        <p>Nedelja: Zatvoreno</p>
                    </div>
                </div>
           </div>
            
          <!--Section B- KONTAKT END-->

        <!--Section C- KONTAKT END-->
        <div class="kontakt-c grid">
            <div class="img-kontakt">
                <img src="images/schneider-auto-registracija-vozila-kontakt.jpg" alt="Schneider registracija automobila pranje vozila">
                </div>
            <div class="lokacija">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1191.277552876186!2d20.445524457649665!3d44.75324602296837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a718da1644c13%3A0xb3300dca749bd0c0!2sAuto%20Perionica%20Istobal!5e0!3m2!1sen!2srs!4v1625818554563!5m2!1sen!2srs" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
           
       <!--Section C- KONTAKT END-->

      <!--FOOTER-->
         <?php 
                include('includes/inc-footer.php');
            ?>
      <!--FOOTER End-->

      

    </main>


    <script src="main.js"></script>
</html>