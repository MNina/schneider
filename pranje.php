<!DOCTYPE html>
<html lang="rs">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Schneider auto centar | Beograd | Registracija automobila | Pranje automobila | Kafic | Tehnicki pregled</title>
        <link href="main.css" rel="stylesheet" type="text/css">
        <link href="includes/pranje.css" rel="stylesheet" type="text/css">

        <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
        
       <meta name="keywords" content="schneider, auto, centar, pranje, automobila, registracija, tehnicki, pregled, auto ,rakovica, beograd">
       <meta name="description" content="Schneider auto centar d.o.o nalazi se u ulici Patrijarha Dimitrija 1a, 11090, Beograd. Nudimo usluge registracije vozila, osiguranje, tehnički pregled, pranje automobila. Dok se naš stručni tim bavi Vašim vozilom možete uživati u prijatnom ambijentu našeg kafića.">
       <meta name="author" content="MDev">
        
    </head>

    <main>
         <!--HEADER-->
         <?php 
                include('includes/inc-header.php');
         ?>
          <!--HEADER End-->



           <!--Section A pranje-->
         <div class="pranje-main">
            
            <h1>Pranje vozila</h1>
            <picture>
                    <source media="(min-width: 768px)"srcset="images/pranje/schneider-d.o.o-pranje-automobila.jpg">
                   
                    <img src="images/pranje/schneider-auto-registracija-vozila33.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
            </picture>
         </div>       
           <!--section A pranje End-->

           <!--section B pranje End-->
               <div class="section-b-pranje">
                    <div class="container">
                        <div class="main-b-pranje grid">
                             <div class="b-box-pranje">
                               <i class="fas fa-parking pranje-icon"></i>
                                 <h3>Obezbedjen parking za klijente</h3>
                             </div>
                             <div class="b-box-pranje">
                               <i class="fas fa-pump-soap pranje-icon"></i>
                                 <h3>Kvalitetni preparati</h3>
                             </div>
                             <div class="b-box-pranje">
                             <i class="fas fa-car pranje-icon"></i>
                                 <h3>Voskiranje</h3>
                             </div>
                             <div class="b-box-pranje">
                                <i class="fas fa-hands-wash pranje-icon"></i>
                                 <h3>Dubinsko pranje</h3>
                             </div>
                             <div class="b-box-pranje b-box-5">
                                 <i class="fas fa-handshake pranje-icon"></i>
                                 <h3>Kvalitetna usluga</h3>
                             </div>
                             <div class="b-box-pranje b-box-5">
                             <i class="far fa-smile pranje-icon"></i></i>
                                 <h3>Zadovoljni klijenti</h3>
                             </div>

                        </div>
                    </div>
               </div>
           <!--section B pranje End-->
           
           <!--section D pranje-->
           <div class="section-d-pranje">
                  <div class="container">
                      <div class="main-d-pranje grid">
                            <div class="d-box-pranje2">
                                <h4>Schneider pranje automobila</h4>
                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Numquam ab aspernatur dolorem incidunt 
                                    voluptate nisi excepturi repudiandae vitae! Maxime earum autem dolore sit eligendi doloremque nisi.
                                </p>
                                <p>Eveniet perspiciatis veritatis id a quas est cum iusto molestiae perferendis? Esse, voluptatum nulla! 
                                    Itaque, corrupti! Incidunt corporis eligendi in. Sapiente, nulla!
                                </p>
                                <p> Ullam, provident sint ipsum iusto 
                                    dolor sed ab, sapiente repellendus atque quia voluptatibus itaque libero ducimus! Tempora quis iusto 
                                    iste dolorem debitis ex pariatur minima, quibusdam optio voluptatum suscipit adipisci ipsam est provident 
                                    vel saepe qui mollitia. Cum distinctio, unde dolorum tempora provident, accusantium modi porro ex maxime, 
                                    enim quibusdam ratione expedita!
                                </p>
                                <ul>
                                    <li>Lorem ipsum dolor.</li>
                                    <li>Fuga, odio.</li>
                                    <li>Amet consectetur.</li>
                                    <li>Sit amet consectetur.</li>
                                </ul>
                            </div>
                            <div class="d-box-pranje1">
                               <img src="images/pranje/schneider-pranje-automobila.jpg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                            </div>
                        </div>
                   </div>
              </div>
           <!--section D pranje End-->

           <!--section C pranje-->

             <div class="section-c-pranje">
                 <div class="container">
                     <div class="c-main-pranje grid">
                        <div class="c-box-pranje">
                            <img src="images/pranje/schneider-d.o.o-pranje-automobila-box-2.jpeg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                        </div>
                        <div class="c-box-pranje">
                          <img src="images/pranje/schneider-d.o.o-pranje-automobila-box1.jpeg" alt="schneider auto centar, tehnicki pregled, registracija pranje auta">
                       </div>
                     </div>
                 </div>
            
             </div>

           <!--section C pranje End-->

           



      <!--FOOTER-->
         <?php 
                include('includes/inc-footer.php');
            ?>
      <!--FOOTER End-->

      

    </main>


    <script src="main.js"></script>
</html>